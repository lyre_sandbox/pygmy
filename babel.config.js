const plugins = [];
if (process.env.NODE_ENV !== "production") {
    plugins.push("react-refresh/babel");
    plugins.push("jotai/babel/plugin-debug-label");
    plugins.push("jotai/babel/plugin-react-refresh");
}
module.exports = {
    presets: ["@babel/preset-env", ["@babel/preset-react", {"runtime": "automatic"}], "jotai/babel/preset"],
    plugins: plugins,
};
