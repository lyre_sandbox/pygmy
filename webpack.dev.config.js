/* eslint-disable @typescript-eslint/no-var-requires */
const path = require("path");
const webpack = require("webpack");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const ESLintPlugin = require("eslint-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const dotenv = require("dotenv");
const fs = require("fs");

const isDev = "development";
const PORT = process.env.PORT || 3005;

const baseEnvPath = path.join(__dirname) + "/.env";
const devEnvPath = path.join(__dirname) + "/.env.dev";

let fileEnv = { "IS_DEV": true };
const finalPath = fs.existsSync(devEnvPath) ? devEnvPath : baseEnvPath;
if (fs.existsSync(baseEnvPath))
  fileEnv = { ...fileEnv, ...dotenv.config({ path: finalPath }).parsed };

const envKeys = Object.keys(fileEnv).reduce((prev, next) => {
  prev[`process.env.${next}`] = JSON.stringify(fileEnv[next]);
  return prev;
}, {});

const plugins = [
  new webpack.DefinePlugin(envKeys),
  new CleanWebpackPlugin(),
  new BundleAnalyzerPlugin({ analyzerMode: process.env.STATS || "disabled" }),
  new HtmlWebpackPlugin({
    template: "./src/index.html",
    favicon: "./src/images/favicon.ico"
  }),
  new MiniCssExtractPlugin(),
  new ESLintPlugin({
    extensions: ["js", "jsx", "ts", "tsx"],
    failOnError: false,
    cache: true,
    emitWarning: true
  })
];


if (process.env.SERVE) {
  plugins.push(new ReactRefreshWebpackPlugin());
}

module.exports = (
  {
    mode: isDev,
    entry: "./src/main.tsx",
    plugins,
    output: {
      filename: "main.js",
      assetModuleFilename: "images/[hash][ext][query]",
      path: path.resolve(__dirname, "build"),
      publicPath: "/"
    },
    resolve: {
      extensions: ["", ".js", ".jsx", ".tsx", ".ts"],
      plugins: [new TsconfigPathsPlugin()]
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          exclude: /node_modules/
        },
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.css$/i,
          use: [MiniCssExtractPlugin.loader, "css-loader", "postcss-loader"]
        },
        {
          test: /\.(s[ca])ss$/i,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: "css-loader",
              options: {
                importLoaders: 3,
                modules: {
                  localIdentName: "[name]_[local]__[hash:base64:5]",
                  mode: (resourcePath) => {
                    if (/global.sass$/i.test(resourcePath)) return "global";
                    return "local";
                  }
                }
              }
            },
            // "postcss-loader",
            {
              loader: "sass-loader"
            },
            {
              loader: "sass-resources-loader",
              options: {
                hoistUseStatements: false,
                resources: [
                  path.resolve(__dirname, "./src/styles/variables/*.sass"),
                  path.resolve(__dirname, "./src/styles/mixins/*.sass")
                ]
              }
            }
          ]
        },
        {
          test: /.svg$/i,
          issuer: /\.[jt]sx?$/,
          use: [
            {
              loader: require.resolve("@svgr/webpack"),
              options: {
                prettier: false,
                svgo: false,
                svgoConfig: {
                  plugins: [{ removeViewBox: false }]
                },
                titleProp: true,
                ref: true
              }
            },
            {
              loader: require.resolve("file-loader"),
              options: {
                name: "static/media/[name].[hash].[ext]"
              }
            }
          ]
        },
        {
          test: /.(jpe?g|png|gif)$/i,
          type: "asset"
        }
      ]
    },
    devtool: "source-map",
    devServer: {
      static: ["./build"],
      port: PORT,
      hot: true,
      client: {
        logging: "info",
        overlay: {
          warnings: false
        }
      },
      historyApiFallback: true
    }
  });
