declare module '*.gif' {
  const src: string;
  export default src;
}

declare module '*.jpg' {
  const src: string;
  export default src;
}

declare module '*.jpeg' {
  const src: string;
  export default src;
}

declare module '*.png' {
  const src: string;
  export default src;
}
declare module '*.svg?url' {
  const content: string;
  export default content;
}
declare module '*.svg' {
  import * as React from 'react';
  const src: string;

  export const ReactComponent: React.FunctionComponent<React.SVGProps<SVGSVGElement> & { title?: string }>;
  export default src;
}
declare module '*.sass' {
  const content: Record<string, string>;
  export default content;
}
