import { FC } from 'react';
import Card from './card/card';
import c from './gallery.module.sass';
import CardLine from './card/card_line/card_line';
import pygmy from '../../../images/png/pygmy.png';
import cards from './test.data';
import '~styles/global.sass';

const Gallery: FC = () => {
  return (
    <div>
      {/*<div className={'hello'}>*/}
      {/*  <FormikTest />*/}
      {/*</div>*/}
      <div className={c.wrapper}>
        {cards.map((c, i) => (
          <Card
            key={i}
            img={pygmy}
            content={<CardLine creator={c.creator} resolution={c.resolution} date={c.date} />}
          />
        ))}
      </div>
    </div>
  );
};

export default Gallery;
