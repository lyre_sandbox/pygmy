import { FC } from 'react';
import c from './card_line.module.sass';
import Avatar from '../../avatar/avatar';
import { GalleryImg } from '../../test.data';

interface Props extends Omit<GalleryImg, 'img'> {
}

const CardLine: FC<Props> = ({ resolution, creator, date }) => {
  return (
    <div className={c.root}>
      <div className={c.user}>
        <Avatar src={creator.avatar} className={c.avatar} />
        <div>
          <span>
          {creator.name}
            <br />
          <span className={c.date}>
            {date}
          </span>
        </span>
        </div>
      </div>
      <div className={c.info}>
        <span>
          {resolution.name}
        </span>
        <span>
          {resolution.pixels}
        </span>
      </div>
    </div>
  );
};

export default CardLine;
