import c from './card.module.sass';
import { FC, ReactNode } from 'react';

interface Props {
  content: ReactNode;
  img: string
}

const Card: FC<Props> = ({ content, img }) => {
  return (
    <div className={c.root}>
      <div className={c.img}>
        <img src={img} alt={'pic'}/>
      </div>
      <div className={c.content}>
        {content}
      </div>
    </div>
  );
};

export default Card;
