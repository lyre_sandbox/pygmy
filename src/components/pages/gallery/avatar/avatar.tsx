import { FC } from 'react';
import c from './avatar.module.sass';
import cs from '../../../../utils/cs';

interface Props {
  src: string;
  size?: 'small' | 'medium' | 'large';
  className?: string;
}

const Avatar: FC<Props> = ({ src, size = 'small', className }) => {
  return (
    <img alt={'pic'} src={src} className={cs(c.root, c[size], className || '')} />
  );
};

export default Avatar;
