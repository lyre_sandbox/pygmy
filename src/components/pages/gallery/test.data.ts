import pygmy from '../../../images/png/pygmy.png';

interface ICreator {
  avatar: string;
  name: string;
}

export interface GalleryImg {
  img: string;
  creator: ICreator;
  date: string;
  resolution: {
    name: string
    pixels: string
  };
}

const cards: GalleryImg[] = [
  {
    img: pygmy,
    creator: { avatar: pygmy, name: 'Maxim' },
    date: '23 September at 9:20',
    resolution: { name: 'Full HD', pixels: '1920x1280' }
  },
  {
    img: pygmy,
    creator: { avatar: pygmy, name: 'Maxim' },
    date: '23 September at 9:20',
    resolution: { name: 'Full HD', pixels: '1920x1280' }
  },
  {
    img: pygmy,
    creator: { avatar: pygmy, name: 'Maxim' },
    date: '23 September at 9:20',
    resolution: { name: 'Full HD', pixels: '1920x1280' }
  },
  {
    img: pygmy,
    creator: { avatar: pygmy, name: 'Maxim' },
    date: '23 September at 9:20',
    resolution: { name: 'Full HD', pixels: '1920x1280' }
  },
  {
    img: pygmy,
    creator: { avatar: pygmy, name: 'Maxim' },
    date: '23 September at 9:20',
    resolution: { name: 'Full HD', pixels: '1920x1280' }
  },
  {
    img: pygmy,
    creator: { avatar: pygmy, name: 'Maxim' },
    date: '23 September at 9:20',
    resolution: { name: 'Full HD', pixels: '1920x1280' }
  }
];

export default cards;
