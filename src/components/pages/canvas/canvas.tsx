import s from './canvas.sass';
import DrawingCanvas from '../../containers/drawing_canvas/drawing_canvas';

const Canvas = () => {

  return (
    <div className={s.wrapper}>
      <DrawingCanvas />
      <DrawingCanvas />
    </div>
  );
};

export default Canvas;
