import '../../styles/normalize.css';
import './app.sass';
import '../../styles/global.sass';
import Layout from '../containers/layout/layout';
import { Route, Router as Wouter, Switch } from 'wouter';
import { IRoute } from '../../routes';
import { FC, useMemo } from 'react';
import Toolbar from '../containers/layout/toolbar/toolbar';
import { createClient, Provider } from 'urql';

interface Props {
  routes: IRoute[];
}

const client = createClient({
  url: 'https://movies.neo4j-graphql.com/'
});

const App: FC<Props> = ({ routes }) => {
  const links = useMemo(() => {
    return routes.map(r => {
      const pathParts = r.path.split('/');
      const linkTitle = pathParts[pathParts.length - 1];
      return { path: r.path, title: linkTitle };
    });
  }, [routes]);

  return (
    <Provider value={client}>
      <Wouter>
        <Layout
          Toolbar={<Toolbar links={links} />}
        >
          <Switch>
            {routes.map(r => <Route key={r.path} path={r.path}>{r.element}</Route>)}
          </Switch>
        </Layout>
      </Wouter>
    </Provider>
  );
};

export default App;
