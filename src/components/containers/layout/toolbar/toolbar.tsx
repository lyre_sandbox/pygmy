import c from './toolbar.sass';
import Button from '../../../regular/ui_kit/button/button';
import { ReactComponent as BurgerSVG } from '../../../../images/svg/burger.svg';
import logo from '../../../../images/png/pygmy.png';
import { FC } from 'react';
import { Link } from 'wouter';
import { useAtomValue, useSetAtom } from 'jotai';
import { isDocNavOpenAtom } from '../../../app/app.atoms';

interface Props {
  links: { path: string, title: string }[];
}

const Toolbar: FC<Props> = ({ links }) => {
  const isDocOpen = useAtomValue(isDocNavOpenAtom);
  const setDocOpen = useSetAtom(isDocNavOpenAtom);

  return (
    <>
      <div className={c.root}>
        <div className={c.nav}>
          <div className={c.item1}>
            <Button className={c.logoBtn}>
              <img className={c.logo} src={logo} alt={'logo'} />
            </Button>
          </div>
          <div className={c.item2}>
            {links.map((l) => <Link style={{ paddingRight: 5 }} key={l.path} to={l.path}>{l.title}</Link>)}
            {/*<DocNavbar />*/}
          </div>
          <div className={c.item3}>
            <Button
              className={isDocOpen ? c.btnToggledWidth : c.btnWidth}
              onClick={() => setDocOpen(prev => !prev)}
            >
              <BurgerSVG className={c.svg} />
            </Button>
          </div>
        </div>
      </div>
      <div className={c.navOffset} />
    </>
  );
};

export default Toolbar;

