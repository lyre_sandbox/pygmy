import c from './layout.sass'
import { FC, ReactNode } from 'react'
import cs from '../../../utils/cs'
import { useAtomValue } from 'jotai';
import { isDocNavOpenAtom } from '../../app/app.atoms';

interface Props {
  children: ReactNode,
  Toolbar?: ReactNode,
}

const Layout: FC<Props> = ({ children, Toolbar }) => {
const isDocNavOpen = useAtomValue(isDocNavOpenAtom);

  return (
    <>
      <header className={c.header}>
        {Toolbar}
      </header>
      <div className={c.main}>
        <main className={c.content}>{children}</main>
        <div className={cs(c.doc, isDocNavOpen ? c.docOpened : c.docClosed)}></div>
      </div>
    </>
  )
}

export default Layout
