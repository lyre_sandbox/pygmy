import s from './svg_root.sass';
import SvgDots from '../../../regular/draw/svg/svg_dots/svg_dots';
import { useAtom, useSetAtom } from 'jotai';
import {
  curSelectedShapeAtom,
  dotsAtom,
  dotsOnKeyDownAtom,
  dotsOnKeyUpAtom,
  dotsOnMouseMoveAtom,
  shapesAtom
} from '../drawing_canvas.atoms';
import React, { memo, useCallback } from 'react';
import SvgShapes from './svg_shapes/svg_shapes';

const SvgRoot = () => {
  const [dots] = useAtom(dotsAtom);
  const [shapes] = useAtom(shapesAtom);
  const [selectedShape] = useAtom(curSelectedShapeAtom);
  const setSelectedAtom = useSetAtom(curSelectedShapeAtom);
  const dotsMouseMoveChanger = useSetAtom(dotsOnMouseMoveAtom);
  const dotsKeyDownChanger = useSetAtom(dotsOnKeyDownAtom);
  const dotsKeyUpChanger = useSetAtom(dotsOnKeyUpAtom);

  const cb = {
    dotsMouseMove: useCallback((e: React.MouseEvent<SVGSVGElement, MouseEvent>) => {
      const { x, y } = e.currentTarget.getBoundingClientRect();
      dotsMouseMoveChanger([e.clientX - x, e.clientY - y]);
    }, [dotsMouseMoveChanger]),
  };

  return (
    <svg
      className={s.root}
      onMouseDown={dotsKeyDownChanger}
      onMouseUp={dotsKeyUpChanger}
      onMouseMove={cb.dotsMouseMove}
    >
      <SvgShapes
        shapes={shapes}
        selectedShape={selectedShape}
        onSelect={setSelectedAtom}
      />
      <SvgDots dots={dots} />
    </svg>
  );
};

export default memo(SvgRoot);
