import React, { FC, memo } from 'react';
import SvgShape from './svg_shape/svg_shape';
import { Shape } from '../../drawing_canvas.atoms';

interface Props {
  shapes: Shape[];
  selectedShape?: Shape;
  onSelect?: (shape: Shape) => void;
}

const SvgShapes: FC<Props> = ({ shapes, selectedShape, onSelect }) => {
  return (
    <g>
      {shapes.map((shape) => (
        <SvgShape
          onSelect={onSelect}
          selected={selectedShape}
          key={shape.id}
          shape={shape}
          color={shape.color?.hex}
        />
      ))}
    </g>
  );
};

export default memo(SvgShapes);
