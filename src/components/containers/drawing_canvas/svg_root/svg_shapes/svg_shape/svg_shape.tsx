import React, { FC, memo, useCallback } from 'react';
import { Shape } from '../../../drawing_canvas.atoms';

interface Props {
  shape: Shape,
  selected?: Shape,
  color?: string,
  onSelect?: (shape: Shape) => void
}

const SvgShape: FC<Props> = ({ shape, selected, color = 'black', onSelect }) => {

  const cb = {
    onSelect: useCallback(() => {
      if (onSelect) onSelect(shape);
    }, [shape, onSelect])
  };

  return (
    <g onClick={cb.onSelect}>
      <path
        d={shape.path}
        fill="none"
        opacity={selected && selected.id === shape.id ? '0.3' : '0'}
        stroke="red"
        strokeWidth="12"
      />
      <path
        d={shape.path}
        fill="none"
        stroke={color}
        strokeWidth="3"
      />
    </g>
  );
};

export default memo(SvgShape);
