import { atom } from 'jotai';
import { pointsToPath } from '../../../utils/points_to_path';

//Dependent Elements
const createShapeFromDots = (dots: readonly Point[], color: ShapeColor): Shape =>
  ({
    id: Math.random(),
    path: pointsToPath(dots),
    color
  });
const commitDotsAtom = atom(null, (get, set) => {
  const dots = get(dotsAtom);
  if (dots.length) {
    const shape = createShapeFromDots(dots, get(curShapeColorAtom));
    // set(curSelectedShapeAtom, shape);
    set(curSelectedShapeAtom, undefined);
    set(addShapeToShapes, shape);
    set(dotsAtom, []);
  } else {
    set(curSelectedShapeAtom, undefined);
  }
});

//Data
const isDrawingAtom = atom(false);
export const dotsAtom = atom<readonly Point[]>([]);
export const shapesAtom = atom<Shape[]>([]);
export const shapesHistoryAtom = atom<Shape[][]>([]);
export const curShapeColorAtom = atom<ShapeColor>({ name: 'default', hex: 'black' });
export const curSelectedShapeAtom = atom<Shape | undefined>(undefined);
export const colorsOptionsAtom = atom<ShapeColor[]>([
  { name: 'blue', hex: 'blue' },
  { name: 'red', hex: 'red' },
  { name: 'green', hex: 'green' },
  { name: 'default', hex: 'black' },
  { name: 'yellow', hex: 'yellow' }
]);

//Utils
const shapesChangerAtom = atom(null, (get, set, shapes: Shape[]) => {
  set(shapesAtom, shapes);
  set(shapesHistoryAtom, prev => [...prev, shapes]);
});
const addDotToDotsAtom = atom(null, (_, set, data: Point) => {
  set(dotsAtom, prev => [...prev, data]);
});
export const addShapeToShapes = atom(null, (get, set, shape: Shape) => {
  set(shapesChangerAtom, [...get(shapesAtom), shape]);
});
export const setShapeColorAtom = atom(null, (get, set, color: ShapeColor) => {
    set(curShapeColorAtom, color);
    const selectedShape = get(curSelectedShapeAtom);
    if (selectedShape) {
      set(shapesChangerAtom,
        [...get(shapesAtom).filter(a => a.id !== selectedShape.id), {
          ...selectedShape,
          color
        }]);
    }
  }
);

//Event Handlers
export const dotsOnMouseMoveAtom = atom(null, (get, set, update: Point) => {
  if (get(isDrawingAtom)) {
    set(addDotToDotsAtom, update);
  }
});
export const dotsOnKeyDownAtom = atom(null, (_, set) => {
  set(isDrawingAtom, true);
});
export const dotsOnKeyUpAtom = atom(null, (_, set) => {
  set(isDrawingAtom, false);
  set(commitDotsAtom);
});
export const deleteSelectedShape = atom(null, (get, set) => {
  const selected = get(curSelectedShapeAtom);
  let shapes = get(shapesAtom);
  if (selected) shapes = shapes.filter((a) => a.id !== selected.id);
  set(shapesChangerAtom, shapes);
  set(curSelectedShapeAtom, undefined);
});
export const undoShapeChanger = atom(null, (get, set) => {
  const changeHistory = get(shapesHistoryAtom);
  if (changeHistory.length) {
    changeHistory.pop()
    set(shapesAtom, changeHistory[changeHistory.length - 1] || [])
    set(shapesHistoryAtom, changeHistory);
  }
})

//types
type Colors = 'black' | 'blue' | 'green' | 'red' | 'yellow';
export type ShapeColor = { name: string, hex: Colors };
export type Point = readonly [number, number];
export type Shape = {
  id: string | number;
  path: string;
  color?: ShapeColor;
};

if (process.env.IS_DEV) {
  dotsAtom.debugLabel = 'dots'
  shapesAtom.debugLabel = 'shapes'
  shapesHistoryAtom.debugLabel = 'shapes History'
  curShapeColorAtom.debugLabel = 'current shape color'
}
