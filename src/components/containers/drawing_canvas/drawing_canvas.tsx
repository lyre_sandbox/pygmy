import { Provider } from 'jotai';
import SvgRoot from './svg_root/svg_root';
import CanvasControls from './canvas_controls/canvas_controls';
import s from './drawing_canvas.sass';

const DrawingCanvas = () => {
  return (
    <Provider>
      <div className={s.wrapper}>
        <CanvasControls />
        <SvgRoot />
      </div>
    </Provider>
  );
};

export default DrawingCanvas;
