import React, { memo } from 'react';
import { useAtom, useAtomValue, useSetAtom } from 'jotai';
import {
  colorsOptionsAtom,
  curSelectedShapeAtom,
  deleteSelectedShape,
  shapesAtom,
  shapesHistoryAtom,
  undoShapeChanger
} from '../drawing_canvas.atoms';
import ColorOptions from './color_options/color_options';
import s from './canvas_controls.sass';

const CanvasControls = () => {
  const shapeHistory = useAtomValue(shapesHistoryAtom);
  const [shapes] = useAtom(shapesAtom);
  const [selected] = useAtom(curSelectedShapeAtom);
  const [colors] = useAtom(colorsOptionsAtom);
  const setShapes = useSetAtom(shapesAtom);
  const onDeleteSelected = useSetAtom(deleteSelectedShape);
  const onUndoShape = useSetAtom(undoShapeChanger);

  return (
    <div className={s.root}>
      <div>
        <button onClick={onDeleteSelected} disabled={!selected}>Remove Selected</button>
        <button onClick={() => setShapes([])} disabled={!shapes.length}>Remove All</button>
        <button onClick={onUndoShape} disabled={!shapeHistory.length}>Undo</button>
      </div>
      <ColorOptions colors={colors} />
    </div>
  );
};

export default memo(CanvasControls);
