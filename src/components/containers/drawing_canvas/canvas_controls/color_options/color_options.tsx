import { FC } from 'react';
import { ShapeColor, curShapeColorAtom, setShapeColorAtom } from '../../drawing_canvas.atoms';
import { useAtom, useSetAtom } from 'jotai';


interface Props {
  colors: ShapeColor[];
}

const ColorOptions: FC<Props> = ({ colors }) => {
  const [curColor] = useAtom(curShapeColorAtom);
  const setShapeColor = useSetAtom(setShapeColorAtom);
  return (
    <div>
      {colors.map(c => (
          <button
            key={c.name}
            onClick={() => setShapeColor(c)}
            disabled={curColor.name === c.name}
          >
            {c.name}
          </button>
        )
      )}
    </div>
  );
};

export default ColorOptions;
