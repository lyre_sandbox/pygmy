import { ErrorMessage, Field, Form, Formik } from 'formik';
import { regexEmail } from '../../../utils/regex';
import FormikInput from './input/formik_input';
import Stack from '../ui_kit/stack/stack';

const FormikTest = () => (
  <div>
    <h1>Anywhere in your app!</h1>
    <Formik
      initialValues={{ email: '', password: '' }}
      validate={values => {
        const errors: { email?: string } = {};
        if (!values.email)
          errors.email = 'Required';
        else if (!regexEmail.test(values.email))
          errors.email = 'Invalid email address';
        return errors;
      }}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 400);
      }}
    >
      {({ isSubmitting }) => (
        <Form>
          <Stack spacing={'sm'}>
            <FormikInput name={'email'} />
            <Field type="password" name="password" />
            <ErrorMessage name="password" component="div" />
            <button type="submit" disabled={isSubmitting}>
              Submit
            </button>
          </Stack>
        </Form>
      )}
    </Formik>
  </div>
);

export default FormikTest;
