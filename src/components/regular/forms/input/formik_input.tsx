import { FC, ReactNode } from 'react';
import { ErrorMessage, Field } from 'formik';
import Stack from '../../ui_kit/stack/stack';

interface Props {
  name: string;
  labelEl?: ReactNode;
}

const Label = () => {
  return (
    <div>
      label:
    </div>
  )
}

const FormikInput: FC<Props> = ({ name, labelEl = <Label /> }) => {
  return (
    <Stack spacing={'none'}>
      {labelEl}
      <Field type="email" name={name} />
      <ErrorMessage name="email" component="div" />
    </Stack>
  )
    ;
};

export default FormikInput;
