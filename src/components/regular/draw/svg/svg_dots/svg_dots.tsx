import { FC, memo } from 'react';

interface Props {
  dots: readonly (readonly [number, number])[];//x, y
  fill?: string;
  r?: string;
}

const SvgDots: FC<Props> = ({ dots, r = '2', fill = '#aaa' }) => {
  return (
    <g>
      {dots.map(([x, y], i) =>
        <circle key={i} cx={x} cy={y} r={r} fill={fill} />
      )}
    </g>
  );
};

export default memo(SvgDots);
