import { DetailedHTMLProps, FC } from 'react'

interface IconProps
  extends DetailedHTMLProps<
    React.ImgHTMLAttributes<HTMLImageElement>,
    HTMLImageElement
  > {
  src: string
  wh?: string
}

const Icon: FC<IconProps> = (props) => {
  if (props.wh) {
    const wh = props.wh.split(' ')
    const width = parseInt(wh[0])
    const height = parseInt(wh[1])
    return (
      <img
        {...props}
        alt={props.alt || 'pic'}
        width={width}
        height={height ? height : width}
      />
    )
  }
  return (
    <img
      {...props}
      draggable={false}
      alt={props.alt || 'pic'}
      width={props.width || '40px'}
      height={props.height || '40px'}
    />
  )
}

export default Icon
