import { FC, ReactNode, useMemo } from 'react';
import cs from '../../../../utils/cs';
import s from './stack.sass';

interface Props {
  children: ReactNode;
  spacing?: 'none' | 'sm' | 'md' | 'lg';
}

const Stack: FC<Props> = ({ children, spacing }) => {
  const c = useMemo(() => ({
    spacing: spacing ? `.spacing-${spacing}` : '.spacing-sm'
  }), [spacing]);
  return (
    <div className={cs(s.root, c.spacing)}>
      {children}
    </div>
  );
};

export default Stack;
