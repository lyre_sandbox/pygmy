import c from './button.module.sass';
import { DetailedHTMLProps, FC } from 'react';
import cs from '../../../../utils/cs';

type IconButtonProps = DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement>

const Button: FC<IconButtonProps> = ({ className, ...props }) => {
  return (
    <button className={cs(className ? className : c.default, c.root)} {...props}>
      {props.children}
    </button>
  );
};

export default Button;
