import { ReactNode } from 'react';
import NotFound from './components/pages/not_found/not_found';
import Gallery from './components/pages/gallery/gallery';
import UrqlTest from './components/pages/urql_test/urql_test';
import Canvas from './components/pages/canvas/canvas';

export const HOME_ROUTE = '/';
export const GALLERY_ROUTE = '/gallery';
export const URQL_ROUTE = '/urql';
export const CANVAS_ROUTE = '/canvas';

const routes: IRoute[] = [
  {path: HOME_ROUTE, element: 'main'},
  {path: GALLERY_ROUTE, element: <Gallery />},
  {path: URQL_ROUTE, element: <UrqlTest />},
  {path: CANVAS_ROUTE, element: <Canvas />},
  {path: '', element: <NotFound />},
];

export interface IRoute {
  path: string,
  element: ReactNode,
}

export default routes;
