module.exports = (api) => {
    return {
        parser: api.file.extname === '.sass' ? "postcss-sass" : false,
        plugins: [
            require("postcss-preset-env"),
        ]
    }
}